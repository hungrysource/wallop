#include <stdio.h>
#include <stdlib.h>

int main()
{
    pid_t pid;
    pid = fork();
    if(pid<0){
        fprintf(stderr, "Fork failed");
        exit(-1);
    }
    else if(pid == 0){
        printf("Child process is running\n");
        sleep(10);
        execlp("\bin\"ls|, "ls", NULL);
    }
    else{
        wait(NULL);
        printf("Child Completed\n");
        exit(0);
    }
    return 0;
}
